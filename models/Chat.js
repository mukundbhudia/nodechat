/* Chat model. */

var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var chatSchema = mongoose.Schema({
    name: { type: String, default: null },
    dateCreated: { type: Date, default: Date.now },
    participants: [{type: Schema.Types.ObjectId, ref: 'User'}],
    messages: [{
        from: String,
        to: String,
        isSent: Boolean,
        timeStamp: Date,
        message: String, //TODO: may change this to a Mixed type
    }]
});

module.exports = mongoose.model('Chat', chatSchema);
