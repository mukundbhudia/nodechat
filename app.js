var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
// var logger = require('morgan');
var logger = require('./logger');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoose = require('mongoose');

var routes = require('./routes/index');
var users = require('./routes/users');
var login = require('./routes/login');
var lib = require('./lib');
var auth = require('./auth');

var app = express();
var port = process.env.PORT || 3000;

var http = require('http').Server(app);
var io = require('socket.io')(http);

var User = require('./models/User');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
// app.use(logger('dev'));
app.use(require('morgan')('dev', {
    "stream": logger.stream
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'shhhhh1234',
    resave: false,
    saveUninitialized: true
}));

// app.use('/', routes);
app.use('/users', users);
app.use('/login', login);

var mongoURI = process.env.MONGOLAB_URI || 'mongodb://localhost/nodechat';

mongoose.connect(mongoURI, function(err){
    if (err) {
        logger.error("Connection to DB error: " + err + "\n");
    } else {
        logger.info("Connection to DB successful \n");
    }
});

var sess;

app.get('/', function(req, res) {
    sess = req.session;
    if (sess.useremail) {
        logger.info('User ' + sess.useremail + ' detected');
        res.render('index', {
            title: 'Hi ' + sess.userfirstname,
            userID: sess.userID,
            userEmail: sess.useremail,
            userFirstName: sess.userfirstname,
            userLastName: sess.userlastname,
            appName: 'nodechat'
        });
        logger.info(sess.useremail + ' has logged in');
    } else {
        res.location('/login');
        res.redirect('/login');
    }
});

app.get('/login', function(req, res) {
    sess = req.session;
    res.render('login', { title: 'Node Chat', loginInfo: '' });
});

app.post('/login', function(req, res) {
    var sess = req.session;
    var userEmail = req.body.useremail;
    var userPassword = req.body.userpassword;
    var userStatus = req.body.loginstatus;
    var backPath = req.body.backPath;   //the URL path the user was on prior to logging in

    User.find({email: userEmail}).exec( function(err, doc){
        if (doc[0]) {   //If a result exists then the correct user has logged in
            var foundUser = doc[0].toObject(); //Need to convert to JSON object
            //First we check that only one user exists with the username
            if (doc.length === 1 && userEmail === foundUser.email) {
                //Then we authenticate the user hashing the given password and comparing it
                //to the hased pasword stored in the database
                auth.checkPassword(userPassword, foundUser.password, function(passRes) {
                    if (passRes === true) { //hashed passwords are the same
                        sess.useremail = foundUser.email;
                        sess.userfirstname = foundUser.firstName;
                        sess.userlastname = foundUser.lastName;
                        sess.userID = foundUser._id;
                        sess.userstatus = userStatus;
                        res.cookie('loginStatus', sess.userstatus);
                        res.location('/'); //Send user back to where they were
                        res.redirect('/');
                        logger.info(sess.useremail + ' has logged in with status: ' + sess.userstatus);
                    } else {
                        logger.warn("Unsuccessful login (wrong password for user)," +
                        " sending back to login page...");
                        res.render('login', {
                            title: 'nodechat',
                            loginInfo: 'Incorrect password, please try again.'
                        });
                    }
                });

            } else {
                //Technically this case should not occur as the email address for a user is
                //checked for uniqueness upon sign up
                logger.error("Unsuccessful login, multiple user found with email: " + userEmail);
            }

        } else { //No user matches the db query using the given email address
            logger.warn("Unsuccessful login (user not exist), sending back to login page...");
            res.render('login', {
                title: 'nodechat',
                loginInfo: "The given email does not exist in our records, please " +
                " check the email address and try again."
            });
        }
    });

});

app.get('/logout', function(req, res) {
    var sess = req.session;
    var userToLogOut = sess.useremail;
    sess.destroy(function(err) {
        if (err) return logger.error(err);
        logger.info("User: " + userToLogOut + " has logged out.");
        res.location('/');
        res.redirect('/');
    });
});

app.get('/signup', function(req, res) {
    sess = req.session;
    res.render('signup', { title: 'Node Chat', loginInfo: '' });
});

/* POST signup page. */
app.post('/signup', function(req, res) {
    var sess = req.session;

    var userFirstName = req.body.userfirstname;
    var userLastName = req.body.userlastname;
    var userEmail = req.body.useremail;
    var userPassword = req.body.userpassword;
    var backPath = req.body.backPath;   //the URL path the user was on prior to logging in

    var userToSignup = new User({
        firstName: userFirstName,
        lastName: userLastName,
        email: userEmail
    });

    if (userFirstName && userLastName && userPassword && userEmail) {

        User.find({email: userEmail}, function(err, doc){
            if (doc[0]) {   //If a result exists then the correct user has logged in
                var foundUser = doc[0].toObject(); //Need to convert to JSON object
                if (doc.length === 1 && userEmail === foundUser.email) {
                    res.render('signup', {
                        title: 'nodechat',
                        signUpInfo: 'The email address ' +
                        userEmail + ' is already taken, please try again.'
                    });
                    logger.info("Sign up unsuccessful, user: " +
                    userEmail + " already exists. Sending back to sign up page...");
                }
            } else {
                auth.generatePassword(userPassword, function(hashedPassword){
                    userToSignup.password = hashedPassword;
                    userToSignup.save(function (err, userToSignup) {
                        if (err) return logger.error(err);
                        logger.info("User: " + userToSignup.getFullName() +
                        " with email: " + userEmail + ' has signed up.');
                        sess.useremail = userToSignup.email;
                        sess.userfirstname = userToSignup.firstName;
                        sess.userlastname = userToSignup.lastName;
                        sess.userID = userToSignup._id;
                        res.location("/"); //Send user back to where they were
                        res.redirect("/");
                    });
                });
            }
        });

    } else {
        res.render('signup', { title: 'nodechat', signUpInfo: 'Please fill out all data' });
    }
});

app.get('/saveChat', function(req, res) {
    sess = req.session; //TODO: auth user
    lib.saveNewChatToDB({participants: req.query.participants}, function(err, data){
        if (err) {
            logger.error(err);
        } else {
            res.json(data);
        }
    });
});

app.get('/getMyChats', function(req, res) {
    sess = req.session; //TODO: auth user
    lib.getUserChats(sess.userID, function(err, data){
        if (err) {
            logger.error(err);
        } else {
            res.json(data);
        }
    });
});

app.get('/findUsers/:userToFind', function(req, res) {
    var userToFind = req.params.userToFind;
    sess = req.session; //TODO: auth user
    lib.findUsers(userToFind, function(err, data){
        if (err) {
            logger.warn(err);
            res.json(err);
        } else {
            res.json(data);
        }
    });
});

//Holds all the users joined at one instant
var rooms = [];

var room = io.of('/room').on('connection', function(socket) {
    var joinedRoom = null;
    socket.on('join room', function(data){
        socket.join(data);
        joinedRoom = data;
        socket.emit('joined', "hi you've joined " + data);
        logger.log('a user has joined: ' + data);
        //We add a user to the rooms array if they do not already exist
        if (!lib.doesItemExistInArray(rooms, joinedRoom)) {
            rooms.push(joinedRoom);
        }
        socket.broadcast.to(joinedRoom).send('Someone has joined the room');
    });
    socket.on('fromclient', function(data) {
        var fromUser = data.from;
        var toUser = data.to;
        logger.debug(JSON.stringify(data));
        lib.saveMessageToDB(data);
        if (joinedRoom) {
            socket.broadcast.to(toUser).emit('new message', data);
        } else {
            socket.send('You are not in a room, join one');
        }
    });
    socket.on('typing', function(data){
        var fromUser = data.from;
        var toUser = data.to;
        socket.broadcast.to(toUser).emit('typing', {from: fromUser, to: toUser, typing: data.typing});
    });
    socket.on('getstatus', function(data){
        logger.debug(JSON.stringify(data));
        var fromUser = data.from;
        var toUser = data.to;
        //Need to check if user exists to send correct status
        if (lib.doesItemExistInArray(rooms, toUser)) {
            socket.broadcast.to(toUser).emit('status', {from: fromUser, to: toUser, status: data.status});
        } else {
            //We send an unknown status back to the chat initiator
            logger.warn("The user: " + toUser + " does not exist, sending unknown status...");
            socket.emit('status', {from: toUser, to: fromUser, status: "unknown"});
        }
    });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

http.listen(port, function(){
    logger.info('Node chat started. Listening on port: ' + port + "...");
});

module.exports = app;
