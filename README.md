# nodechat #

### What is this? ###

Nodechat is a Telegram/Whatsapp/MSN messenger like application that allows you to chat to people who have signed up. Currently web-browser based only.
![Screenshot 2015-05-09 11.38.51.png](https://bitbucket.org/repo/pXRAKr/images/2609260265-Screenshot%202015-05-09%2011.38.51.png)

**Demo** available here: [https://mukundchat.herokuapp.com/](https://mukundchat.herokuapp.com/).

### How do I use the demo? ###
* Once you hit the [URL](http://mukundchat.herokuapp.com/) you'll land on the app hosted by Heroku.
* You'll need to [sign up](https://mukundchat.herokuapp.com/signup) where you can enter any old information as long as you remember at least the email address (which doesn't have to exist for use of the app) and password.
* Once you've signed up, you can open up a private session on your browser (to emulate another users session) and sign up another account.
* Then you'll be able to chat with yourself!
* You could of course find someone else to use the demo with too.

### What you need before you run the app: ###

* MongoDB version >= 3.x.x installed
* NodeJS version >= 0.12.x installed

### How do I install? ###

Once cloned/downloaded cd to directory and:

```
#!shell

npm install
```

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact