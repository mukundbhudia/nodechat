//Library functions for NodeChat

var logger = require('./logger');
var User = require('./models/User');
var Chat = require('./models/Chat');

//Function checks if strings exist in an array
var doesItemExistInArray = function(array, itemToCheck) {
    var foundItems = 0;
    for (var i = 0; i < array.length; i++) {
        if (itemToCheck === array[i]) {
            foundItems++;   //Increase increment if we find a match
        }
    }

    if (foundItems === 0) {
        return false;
    } else {
        return true;
    }
};

//Function to save messgage to DB
var saveMessageToDB = function(msg) {
    var msgToAdd = {
        from: msg.from,
        to: msg.to,
        message: msg.message,
    };
    Chat.findByIdAndUpdate(msg.chatID, {$push: {messages: msgToAdd}}, function(err, data) {
        if (err) {
            logger.error(err);
        } else {
            logger.debug("Message saved: " + data._id +
            ". " + data.messages.length + " messages in chat." );
        }
    });
};

//Function to save a new chat to DB
var saveNewChatToDB = function(chatData, callback) {
    chatToSave = new Chat({
        participants: chatData.participants
    });
    chatToSave.save(function(err, data) {
        if (err) {
            logger.error(err);
            callback && callback("DB save error: \n" + err , null);
        } else {
            logger.info("Saved new chat with ID: " + data._id);
            updateUserChatList(chatData.participants, data._id);
            callback && callback(null, { chatID: data._id });
        }
    });
};

//Gets a list of users chats and corresponding messages when hitting GET URL
var getUserChats = function(userID, callback) {
    User.findById(userID).populate('chats').exec(function(err, data) {
        if (err) {
            logger.error(err);
            callback && callback({error: "unable to get chats"}, null);
        } else {
            User.populate(data, {path: 'chats.participants', model: 'User'}, function(err, data) {
                if (err) return handleError(err);
                logger.debug("Getting chats for user: " + data.firstName +
                " there are " + data.chats.length + " chats for this user.");
                callback && callback(null, data.chats);
            });
        }
    });
};

var updateUserChatList = function(users, chatID) {
    for (var i = 0; i < users.length; i++) {
        User.findByIdAndUpdate(users[i], {$push: {chats: chatID}}, function(err, data) {
            if (err) {
                logger.error(err);
            } else {
                logger.debug("User updated: " + users[i] +
                ". " + data.chats.length + " chats for user." );
            }
        });
    }
};

var findUsers = function(userToFind, callback) {
    if (userToFind.length >= 3) {
        logger.debug("Searching for users containing: " + userToFind + "...");
        User.find({
            firstName: { $regex: new RegExp("" + userToFind.toLowerCase(), "i") }
        }, {limit: 10})
        .select('firstName lastName').exec(function(err, doc) {
            if (err) return logger.error(err);
            if (doc) {
                var foundUsers = doc;
                //TODO: Strip out db id and __v items
                logger.debug("...found " + doc.length + " users matching " + userToFind);
                callback && callback(null, foundUsers);
            } else {
                callback && callback({error : 'User: ' + userToFind + ' not found'}, null);
            }
        });
    } else {
        callback && callback({
            error : 'User query not long enough, need 3 characters or more'}, null);
    }
};

module.exports = {
    doesItemExistInArray: doesItemExistInArray,
    saveMessageToDB: saveMessageToDB,
    saveNewChatToDB: saveNewChatToDB,
    findUsers: findUsers,
    getUserChats: getUserChats
};
